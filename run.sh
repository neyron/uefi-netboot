#!/bin/bash
# PXE boot with UEFI in qemu
#
# Run the vncviewer on the workstation, e.g.:
# workstation$ watch vncviewer localhost
#
# Make sure to have a tunnel for VNC, e.g.:
# workstation$ ssh <site>.g5k -v -L 5900:<node>:5900
# (replace node and site)
#
sudo-g5k || true
mrproper() {
  echo -n MrProper...
  sudo ip addr del $BR0_IP/$NETMASK dev br0 || true
  sudo killall -9 tcpdump || true
  sudo killall -9 dnsmasq || true
  killall -9 qemu-system-x86_64 || true
  echo
}
trap mrproper ERR
trap mrproper KILL
trap mrproper INT

if [ -z "$OAR_NODE_FILE" ]; then
    cat <<EOF
This script must be run in a job.
EOF
    exit 1
fi

QCOW=debian9-x64-min.qcow2 
if [ -f ./$QCOW ]; then
  echo "Use existing QCOW."
else
  cp -v /grid5000/virt-images/$QCOW .
fi
NETWORK=192.168.1.0
NETMASK=255.255.255.0
VM_IP=192.168.0.1
GATEWAY=192.168.0.254

cat <<EOF
NETWORK=$NETWORK
NETMASK=$NETMASK
VM_IP=$VM_IP
GATEWAY=$GATEWAY
EOF

cat > dnsmasq.conf <<EOF
no-daemon
interface=tap0
port=0

user=dnsmasq
group=dnsmasq

# DHCP pool setup
dhcp-range=$VM_IP,$VM_IP
dhcp-option=option:router,$GATEWAY
dhcp-boot=http://$GATEWAY:8888/start-up.nsh
#dhcp-option=60,HTTPClient
dhcp-option=option:vendor-class,HTTPClient


# TFTP Server setup
enable-tftp
tftp-root=$PWD/

log-dhcp
dhcp-leasefile=/tmp/lease
EOF
if ! grep -q buster /etc/apt/sources.list > /dev/null; then
  sed 's/stretch/buster/g' /etc/apt/sources.list | sudo tee /etc/apt/sources.list.d/buster.list
  sudo apt-get update
fi

SYSLINUX_VERSION=3:6.04~git20190206.bf6db5b4+dfsg1-1
OVMF_VERSION=0~20181115.85588389-3
sudo apt-get install tcpdump webfs syslinux-common=$SYSLINUX_VERSION syslinux-efi=$SYSLINUX_VERSION ovmf=$OVMF_VERSION
grep -q dnsmasq /etc/group || sudo groupadd dnsmasq
grep -q dnsmasq /etc/passwd || sudo useradd dnsmasq

cp -r /usr/lib/syslinux/modules/efi64 .
cp -r /usr/lib/SYSLINUX.EFI/efi64 .
if [ -f OVMF.fd ]; then
  echo Keep existing OVMF.fd file
else
  cp /usr/share/ovmf/OVMF.fd .
fi
sudo tunctl -t tap0 -u $USER
sudo ip addr add $GATEWAY/$NETMASK dev tap0 || true
sudo ip link set dev tap0 up || true

sudo dnsmasq -C dnsmasq.conf &

#kvm -hda ./debian9-x64-min.qcow2 -netdev bridge,id=br0 -device virtio-net-pci,netdev=br0,id=nic1,mac=$VM_MAC -vnc :0 -L . --bios OVMF.fd &
kvm -hda ./debian9-x64-min.qcow2 -net nic -net tap,ifname=tap0,script=/bin/true -vnc :0 -L . --bios OVMF.fd &

webfsd -p 8888 -r $PWD -F -d
#sudo tcpdump -i tap0

